package com.nishtahir.linkbait.controller

/**
 * Interface for controllers used in Webservice
 */
trait IController {

    /**
     * Initialize routes
     */
    abstract void init()

}
